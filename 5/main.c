#include <stdio.h>

/* 1990年1月1日 是星期一 */

// 将闰年的判断封装成函数
int isLeapYear(int year)
{
    return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
}

// 返回从1月1日到输入月份1日之间的天数
int getMonthdays(int month, int leap)
{
    int days = 0;
    int monthdays[] = {31,28+!!leap,31,30,31,30,31,31,30,31,30,31}; //若leap大于1,两次取反为1
    for (int i = 0; i < month; i++)
    {
        days += monthdays[i];
    }
    return days;
}

int main()
{
    int year, month, day, week, leap;
    int sum = 0;
    int i;
    //test

    scanf("%d/%d/%d", &year, &month, &day);

    for (i = 1990; i < year; i++)
    {
        if (isLeapYear)
        {
            sum += 366;
        }
        else
        {
            sum += 365;
        }
    }

    if (isLeapYear)
    {
        leap = 1;
    }
    else
    {
        leap = 0;
    }

    sum += getMonthdays(month, leap);
    sum += day;
    week = sum % 7;
    
    char * weekday[] = {"日", "一", "二", "三", "四", "五", "六"};
    
    printf("%d年%d月%d日是星期%s\n", year, month, day, weekday[week]);
    return 0;
}