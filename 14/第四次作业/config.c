#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include "config.h"

// 读取配置文件
int read_config(const char* filename, Config* config) {
    json_t *root;
    json_error_t error;

    // 打开并解析JSON文件
    root = json_load_file(filename, 0, &error);
    if (!root) {
        fprintf(stderr, "无法打开或解析配置文件: %s\n", error.text);
        return -1;
    }

    // 从JSON对象中提取AK和SK
    json_t *ak_json = json_object_get(root, "AK");
    json_t *sk_json = json_object_get(root, "SK");
    if (!json_is_string(ak_json) || !json_is_string(sk_json)) {
        fprintf(stderr, "配置文件格式错误\n");
        json_decref(root);
        return -1;
    }

    // 复制AK和SK
    config->AK = strdup(json_string_value(ak_json));
    config->SK = strdup(json_string_value(sk_json));

    // 释放JSON对象
    json_decref(root);

    return 0;
}

// 释放配置内存
void free_config(Config* config) {
    if (config->AK) {
        free(config->AK);
    }
    if (config->SK) {
        free(config->SK);
    }
}
