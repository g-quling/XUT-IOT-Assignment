#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <cjson/cJSON.h>

// 主函数
int main(void) {
    CURL *client;
    CURLcode err;
    char *buffer;
    size_t size;

    // 初始化libcurl
    curl_global_init(CURL_GLOBAL_ALL);

    // 初始化一个curl会话
    client = curl_easy_init();
    if (!client) {
        fprintf(stderr, "curl_easy_init() failed\n");
        return 1;
    }

    // 设置HTTP Headers，包括Token
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "X-User-Token:+YbRhXVlup96rn6JKeyCZX9JsxmSb8CL");
    curl_easy_setopt(client, CURLOPT_HTTPHEADER, headers);

    // 设置使用 open_memstream 将响应数据写入内存
    FILE *memstream = open_memstream(&buffer, &size);
    if (!memstream) {
        fprintf(stderr, "open_memstream() failed\n");
        curl_slist_free_all(headers);
        curl_easy_cleanup(client);
        curl_global_cleanup();
        return 1;
    }
    curl_easy_setopt(client, CURLOPT_WRITEDATA, memstream);

    // 第一次请求：获取推荐的古诗词
    curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/sentence");

    err = curl_easy_perform(client);
    fclose(memstream);  // 关闭流，确保缓冲区内容完整
    if (err != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(err));
        curl_slist_free_all(headers);
        curl_easy_cleanup(client);
        curl_global_cleanup();
        free(buffer); // 释放缓冲区
        return 1;
    }

    // 打印接收到的JSON数据
    //printf("%lu bytes received:\n%s\n", (unsigned long)size, buffer);

    // 解析JSON数据
    cJSON *json = cJSON_Parse(buffer);
    free(buffer);  // 解析完JSON后释放缓冲区

    if (!json) {
        fprintf(stderr, "JSON parse error: %s\n", cJSON_GetErrorPtr());
        curl_slist_free_all(headers);
        curl_easy_cleanup(client);
        curl_global_cleanup();
        return 1;
    }

    // 提取推荐的古诗词字段
    cJSON *data = cJSON_GetObjectItemCaseSensitive(json, "data");
    cJSON *content = cJSON_GetObjectItemCaseSensitive(data, "content");
    if (cJSON_IsString(content) && content->valuestring != NULL) {
        printf("推荐的古诗词: %s\n", content->valuestring);
    }

    cJSON_Delete(json);

    // 第二次请求：获取天气情况
    memstream = open_memstream(&buffer, &size);
    if (!memstream) {
        fprintf(stderr, "open_memstream() failed\n");
        curl_slist_free_all(headers);
        curl_easy_cleanup(client);
        curl_global_cleanup();
        return 1;
    }
    curl_easy_setopt(client, CURLOPT_WRITEDATA, memstream);
    curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/info");

    err = curl_easy_perform(client);
    fclose(memstream);  // 关闭流，确保缓冲区内容完整
    if (err != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(err));
        curl_slist_free_all(headers);
        curl_easy_cleanup(client);
        curl_global_cleanup();
        free(buffer); // 释放缓冲区
        return 1;
    }

    // 打印接收到的JSON数据
    //printf("%lu bytes received:\n%s\n", (unsigned long)size, buffer);

    // 解析JSON数据
    json = cJSON_Parse(buffer);
    free(buffer);  // 解析完JSON后释放缓冲区

    if (!json) {
        fprintf(stderr, "JSON parse error: %s\n", cJSON_GetErrorPtr());
        curl_slist_free_all(headers);
        curl_easy_cleanup(client);
        curl_global_cleanup();
        return 1;
    }

    // 提取天气数据字段
    data = cJSON_GetObjectItemCaseSensitive(json, "data");
    cJSON *weatherData = cJSON_GetObjectItemCaseSensitive(data, "weatherData");
    if (cJSON_IsObject(weatherData)) {
        cJSON *temperature = cJSON_GetObjectItemCaseSensitive(weatherData, "temperature");
        cJSON *windDirection = cJSON_GetObjectItemCaseSensitive(weatherData, "windDirection");
        cJSON *windPower = cJSON_GetObjectItemCaseSensitive(weatherData, "windPower");
        cJSON *humidity = cJSON_GetObjectItemCaseSensitive(weatherData, "humidity");
        cJSON *updateTime = cJSON_GetObjectItemCaseSensitive(weatherData, "updateTime");
        cJSON *weather = cJSON_GetObjectItemCaseSensitive(weatherData, "weather");
        cJSON *visibility = cJSON_GetObjectItemCaseSensitive(weatherData, "visibility");
        cJSON *rainfall = cJSON_GetObjectItemCaseSensitive(weatherData, "rainfall");
        cJSON *pm25 = cJSON_GetObjectItemCaseSensitive(weatherData, "pm25");

        printf("天气情况:\n");
        if (cJSON_IsNumber(temperature)) printf("温度: %.1f°C\n", temperature->valuedouble);
        if (cJSON_IsString(windDirection)) printf("风向: %s\n", windDirection->valuestring);
        if (cJSON_IsNumber(windPower)) printf("风力: %d级\n", windPower->valueint);
        if (cJSON_IsNumber(humidity)) printf("湿度: %.1f%%\n", humidity->valuedouble);
        if (cJSON_IsString(updateTime)) printf("更新时间: %s\n", updateTime->valuestring);
        if (cJSON_IsString(weather)) printf("天气: %s\n", weather->valuestring);
        if (cJSON_IsString(visibility)) printf("能见度: %s\n", visibility->valuestring);
        if (cJSON_IsNumber(rainfall)) printf("降雨量: %.1fmm\n", rainfall->valuedouble);
        if (cJSON_IsNumber(pm25)) printf("PM2.5: %.1f\n", pm25->valuedouble);
    }

    cJSON_Delete(json);

    // 清理
    curl_slist_free_all(headers);
    curl_easy_cleanup(client);
    curl_global_cleanup();

    return 0;
}
