#include <gpiod.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "record.h"
#include "control.h"
#include "stt.h"

#define GPIO_LINE 9                // PF9对应的GPIO行编号
#define GPIO_LINE2 8               // PF8对应的GPIO编号
#define GPIO_LINE3 7               // PF7按键对应的GPIO编号

extern snd_pcm_t *capture; // 声明外部变量capture

void handle_event(struct gpiod_line_event *event) {
    static int recording = 0;
    if (event->event_type == GPIOD_LINE_EVENT_FALLING_EDGE) {
        // 检测到下降沿事件（按键按下），开始录音
        if (!recording) {
            printf("key pressed\n");
            start_recording("output.pcm", 4096, 2);
            recording = 1;
        }
    } else if (event->event_type == GPIOD_LINE_EVENT_RISING_EDGE) {
        // 检测到上升沿事件（按键松开），停止录音
        if (recording) {
            printf("key released\n");
            stop_recording();
            recording = 0;
            // 调用STT处理函数
            process_audio_file("output.pcm");
        }
    }
}

void handle_volume_event(struct gpiod_line_event *event, const char* card, const char* selem, int change) {
    if (event->event_type == GPIOD_LINE_EVENT_FALLING_EDGE) {
        long volume = get_playback_volume(card, selem);
        volume += change;
        set_playback_volume(card, selem, volume);
        printf("Volume changed to %ld\n", volume);
    }
}

int main(void) {
    struct gpiod_chip *chip;             // GPIO芯片对象
    struct gpiod_line *line, *line2, *line3; // GPIO行对象
    struct gpiod_line_event event;       // GPIO行事件对象
    int ret;
    const char *pcm_device = "hw:0,0";
    snd_pcm_format_t format = SND_PCM_FORMAT_S16_LE;
    unsigned int rate = 44100;
    int channels = 2;
    snd_pcm_uframes_t frames = 4096;

    // 打开GPIO芯片
    chip = gpiod_chip_open_by_label("GPIOF");
    if (!chip) {
        perror("打开GPIO芯片失败");
        return -1;
    }

    // 获取GPIO行
    line = gpiod_chip_get_line(chip, GPIO_LINE);
    line2 = gpiod_chip_get_line(chip, GPIO_LINE2);
    line3 = gpiod_chip_get_line(chip, GPIO_LINE3);
    if (!line || !line2 || !line3) {
        perror("获取GPIO行失败");
        gpiod_chip_close(chip);
        return -1;
    }

    // 请求GPIO行作为输入并启用双边沿事件检测
    ret = gpiod_line_request_both_edges_events(line, "gpio_key");
    ret |= gpiod_line_request_both_edges_events(line2, "gpio_key2");
    ret |= gpiod_line_request_both_edges_events(line3, "gpio_key3");
    if (ret < 0) {
        perror("请求GPIO行事件失败");
        gpiod_chip_close(chip);
        return -1;
    }

    // 打开并配置PCM设备
    ret = open_pcm_device(&capture, pcm_device, format, rate, channels, frames);
    if (ret != 0) {
        printf("音频设备配置失败，错误码：%d\n", ret);
        return ret;
    }

    printf("等待按键事件...\n");

    // 事件循环
    while (1) {
        // 等待事件发生
        ret = gpiod_line_event_wait(line, NULL);
        if (ret < 0) {
            perror("等待事件失败");
            break;
        }

        // 如果有事件发生，读取并处理事件
        if (ret == 1) {
            ret = gpiod_line_event_read(line, &event);
            if (ret < 0) {
                perror("读取事件失败");
                break;
            }

            // 处理事件
            handle_event(&event);
        }

        ret = gpiod_line_event_wait(line2, NULL);
        if (ret == 1) {
            ret = gpiod_line_event_read(line2, &event);
            if (ret == 0) {
                handle_volume_event(&event, "hw:0", "Master", 5); // 增加音量
            }
        }

        ret = gpiod_line_event_wait(line3, NULL);
        if (ret == 1) {
            ret = gpiod_line_event_read(line3, &event);
            if (ret == 0) {
                handle_volume_event(&event, "hw:0", "Master", -5); // 减少音量
            }
        }

        // 在事件等待循环中调用录音处理函数
        handle_recording(frames);
    }

    // 关闭GPIO芯片
    gpiod_chip_close(chip);

    return 0;
}
