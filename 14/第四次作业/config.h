#ifndef CONFIG_H
#define CONFIG_H

// 结构体定义，用于存储配置信息
typedef struct {
    char* AK;
    char* SK;
} Config;

// 函数声明，用于读取配置文件
int read_config(const char* filename, Config* config);

// 函数声明，用于释放配置内存
void free_config(Config* config);

#endif // CONFIG_H
