#ifndef _HTTP_H_
#define _HTTP_H_

/**
 * @file http.h
 * @brief 提供基本的HTTP GET和POST请求功能。
 */

#include <stddef.h>
#include <curl/curl.h>

/**
 * 发送HTTP GET请求。
 * @param url 请求的URL。
 * @return 成功返回响应内容字符串，失败返回NULL。
 */
char *http_get(const char *url);

/**
 * 发送HTTP POST请求。
 * @param url 请求的URL。
 * @param data 发送的数据。
 * @param data_size 发送数据的大小。
 * @param response_code 用于存储HTTP响应代码的变量的地址。
 * @param headers 自定义请求头。
 * @return 成功返回响应内容字符串，失败返回NULL。
 */
char *http_post(const char *url, const char *data, size_t data_size, long *response_code, struct curl_slist *headers);

#endif // _HTTP_H_
