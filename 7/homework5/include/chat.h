#ifndef _CHAT_H_
#define _CHAT_H_

/**
 * 使用百度云大模型进行对话
 * @param input_text 输入的对话文本
 * @return 成功返回大模型的回复，失败返回NULL
 */
char *chat_with_baidu_model(const char *input_text);

/**
 * 创建百度云大模型会话
 * @param authtoken 认证令牌
 * @return 成功返回会话ID，失败返回NULL
 */
char *create_conversation(const char *authtoken);

#endif // _CHAT_H_
