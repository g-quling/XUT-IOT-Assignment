#ifndef _BAIDU_CLOUD_H_
#define _BAIDU_CLOUD_H_

/**
 * @file baidu_cloud.h
 * @brief 包含与百度云API交互的函数，主要用于语音识别功能。
 */

/**
 * 获取百度云的访问令牌。
 * @param api_key 百度云API Key。
 * @param secret_key 百度云Secret Key。
 * @return 成功返回令牌字符串，失败返回NULL。
 * 使用百度云语音识别之前，必须先调用此函数获取访问令牌。
 */
char *baidu_get_access_token(const char *api_key, const char *secret_key);

/**
 * 从语音识别响应中提取识别结果。
 * @param response 服务器返回的JSON响应。
 * @return 成功返回识别的文本，失败返回NULL。
 * 该函数解析JSON格式的响应，提取出语音识别的文本结果。
 */
char *extract_result(const char *response);

/**
 * 使用百度云语音识别服务。
 * @param filename 包含录音数据的文件名。
 * @param token 访问令牌。
 * @param response_code 用于存储HTTP响应代码的变量的地址。
 * @return 成功返回识别结果，失败返回NULL。
 * 此函数读取音频文件数据，发送到百度云进行语音识别，并返回识别结果。
 */
char *baidu_recognize_speech(const char *filename, const char *token, long *response_code);

#endif // _BAIDU_CLOUD_H_
