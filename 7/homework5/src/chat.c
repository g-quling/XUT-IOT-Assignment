#include "chat.h"
#include "http.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <cjson/cJSON.h>

#define BAIDU_CHAT_API_URL "https://qianfan.baidubce.com/v2/app/conversation/runs"
#define CREATE_CONVERSATION_URL "https://qianfan.baidubce.com/v2/app/conversation"
#define AUTH_TOKEN "bce-v3/ALTAK-QNxARmIMxHrOMzFsRt2rj/f13f9a0170bc57f0ffc72d36d732696ed0f25510"  // 确定的auth token
#define APP_ID "63576019-3feb-423c-bdfc-31e85acff3fd"

char *create_conversation(const char *authtoken) {
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json;charset=utf-8");

    char auth_header[256];
    snprintf(auth_header, sizeof(auth_header), "Authorization: Bearer %s", authtoken);
    headers = curl_slist_append(headers, auth_header);

    cJSON *json_request = cJSON_CreateObject();
    cJSON_AddStringToObject(json_request, "app_id", APP_ID);
    char *request_body = cJSON_PrintUnformatted(json_request);

    long response_code;
    char *response = http_post(CREATE_CONVERSATION_URL, request_body, strlen(request_body), &response_code, headers);

    free(request_body);
    cJSON_Delete(json_request);

    if (response_code != 200 || !response) {
        fprintf(stderr, "Failed to create conversation\n");
        free(response);
        return NULL;
    }

    cJSON *json_response = cJSON_Parse(response);
    free(response);
    if (!json_response) {
        fprintf(stderr, "Failed to parse JSON response\n");
        return NULL;
    }

    cJSON *conversation_id = cJSON_GetObjectItemCaseSensitive(json_response, "conversation_id");
    char *retval = NULL;
    if (cJSON_IsString(conversation_id) && conversation_id->valuestring != NULL) {
        retval = strdup(conversation_id->valuestring);
    } else {
        fprintf(stderr, "conversation_id 字段不存在或为空\n");
    }

    cJSON_Delete(json_response);
    return retval;
}

char *chat_with_baidu_model(const char *input_text) {
    char *conversation_id = create_conversation(AUTH_TOKEN);
    if (!conversation_id) {
        return NULL;
    }

    char url[1024];
    snprintf(url, sizeof(url), "%s", BAIDU_CHAT_API_URL);

    cJSON *json_request = cJSON_CreateObject();
    cJSON_AddStringToObject(json_request, "app_id", APP_ID);
    cJSON_AddStringToObject(json_request, "query", input_text);
    cJSON_AddBoolToObject(json_request, "stream", false);
    cJSON_AddStringToObject(json_request, "conversation_id", conversation_id);

    char *request_body = cJSON_PrintUnformatted(json_request);
    long response_code;

    struct curl_slist *headers = NULL;
    char auth_header[256];
    snprintf(auth_header, sizeof(auth_header), "Authorization: Bearer %s", AUTH_TOKEN);
    headers = curl_slist_append(headers, auth_header);
    headers = curl_slist_append(headers, "Content-Type: application/json");

    char *response = http_post(url, request_body, strlen(request_body), &response_code, headers);

    free(conversation_id);
    free(request_body);
    cJSON_Delete(json_request);

    if (response_code != 200 || !response) {
        fprintf(stderr, "Failed to get response from Baidu Chat API\n");
        free(response);
        return NULL;
    }

    cJSON *json_response = cJSON_Parse(response);
    free(response);
    if (!json_response) {
        fprintf(stderr, "Failed to parse JSON response\n");
        return NULL;
    }

    cJSON *text = cJSON_GetObjectItemCaseSensitive(json_response, "answer");
    printf("%s/n",text);

    char *reply_text = NULL;
    if (cJSON_IsString(text) && text->valuestring != NULL) {
        reply_text = strdup(text->valuestring);
    }

    cJSON_Delete(json_response);
    return reply_text;
}
