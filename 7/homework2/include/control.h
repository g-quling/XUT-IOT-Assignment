#ifndef _CONTROL_H_
#define _CONTROL_H_

#include <stdbool.h>
#include <alsa/asoundlib.h>

/**
 * 获取音频回放音量
 * @param card：声卡名称
 * @param selem：控制项名称
 * @return：成功返回音量值，失败返回负错误码
 */
int get_playback_volume(const char *card, const char *selem);

/**
 * 设置音频回放音量
 * @param card：声卡名称
 * @param selem：控制项名称
 * @param volume：指向要设置的音量值的指针
 * @return：成功返回0，失败返回负错误码
 */
int set_playback_volume(const char *card, const char *selem, long *volume);

/**
 * 获取音频回放音量范围
 * @param card：声卡名称
 * @param selem：控制项名称
 * @param min_volume：指向最小音量的指针
 * @param max_volume：指向最大音量的指针
 * @return：成功返回0，失败返回负错误码
 */
int get_playback_volume_range(const char *card, const char *selem, long *min_volume, long *max_volume);

#endif // _CONTROL_H_
