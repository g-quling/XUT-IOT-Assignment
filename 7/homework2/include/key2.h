#ifndef _KEY2_H_
#define _KEY2_H_

/**
 * 按键2控制
 * @param card: 声卡名称
 * @param selem: 控制项名称
 */
void key2_control(const char *card, const char *selem);

#endif // _KEY2_H_
