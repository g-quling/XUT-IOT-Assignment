#ifndef _KEY1_H_
#define _KEY1_H_

/**
 * 按键1控制录音
 * @param pcm_device: PCM设备名
 * @param channels: 声道数
 * @param sample_rate: 采样率
 * @param filename: 输出文件名
 */
void key1_record_control(const char *pcm_device, unsigned int channels, unsigned int sample_rate, const char *filename);

#endif // _KEY1_H_
