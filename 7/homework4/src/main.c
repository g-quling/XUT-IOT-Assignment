#include "key1.h"
#include "key2.h"
#include "key3.h"
#include <unistd.h>

int main(void)
{
    const char *pcm_device = "hw:0,1";
    unsigned int channels = 2;
    unsigned int sample_rate = 16000;
    const char *filename = "output.pcm";

    // 调用按键1录音控制函数
    key1_record_control(pcm_device, channels, sample_rate, filename);
    // 调用按键2音量增加控制函数
    key2_control("hw:0", "PCM");
    // 调用按键3音量降低控制函数
    key3_control("hw:0", "PCM");

    // 主线程保持运行状态
    while (1)
    {
        sleep(1); // 每秒钟睡眠一次
    }

    return 0;
}
