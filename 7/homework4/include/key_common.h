#ifndef _KEY_COMMON_H_
#define _KEY_COMMON_H_
#include <gpiod.h>

struct gpiod_line *gpio_init(const char *chip_label, unsigned int line_num, const char *consumer);

#endif // _KEY_COMMON_H_
