#ifndef _BAIDU_CLOUD_H_
#define _BAIDU_CLOUD_H_

char *baidu_get_access_token(const char *api_key, const char *secret_key);
char *extract_result(const char *response);
char *baidu_recognize_speech(const char *filename, const char *token, long *response_code);

#endif
