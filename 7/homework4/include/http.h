#ifndef _HTTP_H_
#define _HTTP_H_

#include <stddef.h>

char *http_get(const char *url);
char *http_post(const char *url, const char *data, size_t data_size, long *response_code, const char *contentType);

#endif
