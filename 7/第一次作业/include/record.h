#ifndef _RECORD_H_
#define _RECORD_H_

#include <alsa/asoundlib.h>

// 定义一个结构体来保存音频捕捉的状态
typedef struct
{
    snd_pcm_t *pcm_handle;         // PCM捕捉句柄
    FILE *output_file;             // PCM输出文件句柄
    char *buffer;                  // 缓冲区
    snd_pcm_uframes_t period_size; // 缓冲区帧数
    volatile int recording;        // 标志位，用于控制录音的开始和停止
    pthread_t thread;              // 录音线程
} audio_record_context_t;

/**
 * 初始化音频捕捉
 * @param pcm_device: PCM设备名称
 * @param channels: 声道数
 * @param sample_rate: 采样率
 * @param filename: 输出文件名称
 * @return: 音频捕捉上下文
 */
audio_record_context_t *audio_record_init(const char *pcm_device, unsigned int channels, unsigned int sample_rate, const char *filename);

/**
 * 开始录音
 * @param context: 音频捕捉上下文
 */
void audio_record_start(audio_record_context_t *context);

/**
 * 停止录音
 * @param context: 音频捕捉上下文
 */
void audio_record_stop(audio_record_context_t *context);

#endif // _RECORD_H_
