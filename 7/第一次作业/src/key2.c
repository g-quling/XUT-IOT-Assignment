#include "key2.h"
#include "key_common.h"
#include "control.h"
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#define GPIO_CHIP_LABEL "GPIOF"
#define GPIO_LINE 7
#define VOLUME_INCREMENT_PERCENTAGE 10 // 每次增加的音量百分比
#define CONSUMER "key2"


typedef struct {
    const char *card;
    const char *selem;
} key2_thread_args_t;

/**
 * 增加音量
 * @param card: 声卡名称
 * @param selem: 控制项名称
 */
static void increase_volume(const char *card, const char *selem)
{
    long current_volume;
    long min_volume, max_volume;

    // 获取当前音量
    current_volume = get_playback_volume(card, selem);
    if (current_volume < 0)
    {
        fprintf(stderr, "Failed to get current playback volume\n");
        return;
    }

    // 获取音量范围
    if (get_playback_volume_range(card, selem, &min_volume, &max_volume) < 0)
    {
        fprintf(stderr, "Failed to get playback volume range\n");
        return;
    }

    // 计算新的音量
    long volume_increment = (max_volume - min_volume) * VOLUME_INCREMENT_PERCENTAGE / 100;
    long new_volume = current_volume + volume_increment;
    if (new_volume > max_volume)
    {
        new_volume = max_volume;
    }

    // 设置新的音量
    int err = set_playback_volume(card, selem, &new_volume);
    if (err < 0)
    {
        fprintf(stderr, "Failed to set playback volume: %s\n", snd_strerror(err));
        return;
    }

    // 重新获取当前音量
    current_volume = get_playback_volume(card, selem);
    if (current_volume < 0)
    {
        fprintf(stderr, "Failed to get current playback volume after setting new volume\n");
        return;
    }

    printf("Playback volume increased to %ld (max %ld)\n", current_volume, max_volume);
}

/**
 * 处理按键2状态变化
 * @param line: GPIO线指针
 * @param last_value: 上一次的按键状态
 * @param card: 声卡名称
 * @param selem: 控制项名称
 */
static void handle_key_event(struct gpiod_line *line, int *last_value, const char *card, const char *selem)
{
    int value = gpiod_line_get_value(line);

    if (value != *last_value)
    {
        if (value == 0)
        {
            printf("按键2按下，增加音量\n");
            increase_volume(card, selem);
        }
        else 
        {
            printf("按键2释放\n");
        }
        *last_value = value;
    }
}

/**
 * 按键2控制线程函数
 * @param args: 线程参数
 * @return: NULL
 */
static void* key2_thread_func(void *args)
{
    key2_thread_args_t *thread_args = (key2_thread_args_t*)args;

    // 初始化GPIO按键
    struct gpiod_line *line = gpio_init(GPIO_CHIP_LABEL, GPIO_LINE, CONSUMER);
    if (!line)
    {
        free(thread_args);
        return NULL;
    }

    pthread_detach(pthread_self()); // 在资源初始化后分离线程

    // 获取初始按键状态
    int last_value = gpiod_line_get_value(line);

    // 持续监控按键状态
    while(1)
    {
        handle_key_event(line, &last_value, thread_args->card, thread_args->selem);
        usleep(100000); // 延迟100毫秒
    }

    // 关闭GPIO芯片
    gpiod_chip_close(gpiod_line_get_chip(line));
    free(thread_args); // 释放动态分配的内存
    return NULL;
}

/**
 * 按键2控制
 * @param card: 声卡名称
 * @param selem: 控制项名称
 */
void key2_control(const char *card, const char *selem)
{
    pthread_t key2_thread;
    key2_thread_args_t *args = (key2_thread_args_t*)malloc(sizeof(key2_thread_args_t));
    if (!args)
    {
        perror("内存分配失败");
        return;
    }

    args->card = card;
    args->selem = selem;

    // 创建按键2控制线程
    if (pthread_create(&key2_thread, NULL, key2_thread_func, args) != 0)
    {
        perror("创建按键2控制线程失败");
        free(args); // 如果线程创建失败，释放内存
        return;
    }
}
