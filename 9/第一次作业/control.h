#ifndef CONTROL_H
#define CONTROL_H

#include <alsa/asoundlib.h>

// 设置音频回放开关
// card：声卡名称
// selem：控制项名称
// enable：开关状态
// return：成功返回0,失败返回错误码
int set_playback_switch(const char *card, const char *selem, bool enable);

// 设置音频采集开关
// card：声卡名称
// selem：控制项名称
// enable：开关状态
// return：成功返回0,失败返回错误码
int set_capture_switch(const char *card, const char *selem, bool enable);

// 获取音频回放音量
// card：声卡名称
// selem：控制项名称
// return：当前音量
int get_playback_volume(const char *card, const char *selem);

// 获取音频采集音量
// card：声卡名称
// selem：控制项名称
// return：当前音量
int get_capture_volume(const char *card, const char *selem);

// 设置音频回放音量
// card：声卡名称
// selem：控制项名称
// volume：音量
// return：成功返回设置后的音量,失败返回错误码
int set_playback_volume(const char *card, const char *selem, long volume);

// 设置音频采集音量
// card：声卡名称
// selem：控制项名称
// volume：音量
// return：成功返回设置后的音量值,失败返回错误码
int set_capture_volume(const char *card, const char *selem, long volume);

#endif