#ifndef RECORD_H
#define RECORD_H

#include <alsa/asoundlib.h>

// 获取设备
snd_pcm_t *record_start(const char *name,
                        snd_pcm_format_t format,
                        unsigned int channel,
                        unsigned int rate,
                        snd_pcm_uframes_t *period);

// 关闭设备
void record_stop(snd_pcm_t *capture);

// 开始录音
// snd_pcm_t *start_recording(char **buffer, FILE *pcm_file, snd_pcm_uframes_t *period);

// 停止录音
// void stop_recording(snd_pcm_t *capture, char *buffer, FILE *pcm_file);

#endif