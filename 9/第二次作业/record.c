#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "record.h"

// 开始录音
snd_pcm_t* record_open(const char* name,
                        snd_pcm_format_t format,
                        unsigned int channel,
                        unsigned int rate,
                        snd_pcm_uframes_t* period)
{
    snd_pcm_t *capture; // PCM设备句柄
    snd_pcm_hw_params_t *params; // PCM硬件参数
    int err; // 用于存储错误码
    int dir;

    // 打开PCM设备用于录音（捕捉）
    if ((err = snd_pcm_open(&capture, name, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
        fprintf(stderr, "Error opening PCM device %s: %s\n", name, snd_strerror(err));
        return NULL;
    }

    // 分配参数对象，并用默认值填充
    snd_pcm_hw_params_alloca(&params);
    snd_pcm_hw_params_any(capture, params);

    // 设置参数
    // 设置访问类型：交错模式
    if ((err = snd_pcm_hw_params_set_access(capture, params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
        fprintf(stderr, "Error setting access: %s\n", snd_strerror(err));
        snd_pcm_close(capture);
        return NULL;
    }
    // 设置数据格式：16位小端
    if ((err = snd_pcm_hw_params_set_format(capture, params, format)) < 0) {
        fprintf(stderr, "Error setting format: %s\n", snd_strerror(err));
        snd_pcm_close(capture);
        return NULL;
    }
    // 设置声道数：立体声
    if ((err = snd_pcm_hw_params_set_channels(capture, params, channel)) < 0) {
        fprintf(stderr, "Error setting channels: %s\n", snd_strerror(err));
        snd_pcm_close(capture);
        return NULL;
    }
    // 设置采样率
    if ((err = snd_pcm_hw_params_set_rate_near(capture, params, &rate, &dir)) < 0) {
        fprintf(stderr, "Error setting rate: %s\n", snd_strerror(err));
        snd_pcm_close(capture);
        return NULL;
    }
    printf("sample rate: %d Hz\n", rate);

    // 设置周期大小
    if ((err = snd_pcm_hw_params_set_period_size_near(capture, params, period, &dir)) < 0) {
        fprintf(stderr, "Error setting period size: %s\n", snd_strerror(err));
        snd_pcm_close(capture);
        return NULL;
    }

    // 设置硬件参数
    if ((err = snd_pcm_hw_params(capture, params)) < 0) {
        fprintf(stderr, "Error setting HW params: %s\n", snd_strerror(err));
        snd_pcm_close(capture);
        return NULL;
    }

    // 获取周期大小
    snd_pcm_hw_params_get_period_size(params, period, &dir);

    return capture;
}

// 停止录音
void record_close(snd_pcm_t* capture)
{
    snd_pcm_drain(capture); // 排空PCM设备
    snd_pcm_close(capture); // 关闭PCM设备
}

// 开始录音到文件
FILE* start_recording(const char* filename)
{
    FILE *pcm_file = fopen(filename, "wb");
    if (!pcm_file) {
        perror("Error opening output file");
        return NULL;
    }
    return pcm_file;
}

// 停止录音到文件
void stop_recording(FILE* pcm_file)
{
    if (pcm_file) {
        fclose(pcm_file);
    }
}
