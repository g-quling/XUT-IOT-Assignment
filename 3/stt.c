#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "token.h"
#include "http.h"

//读取音频文件
//file: 音频文件路径
//size: 音频文件大小
//return: 音频文件内容, NULL 表示失败
char* load_audio_file(const char* file, size_t* size)
{
    //打开音频文件
    FILE* fp = fopen(file, "rb");
    if (!fp) {
        perror(file);
        return NULL;
    }
    //获取文件大小
    fseek(fp, 0, SEEK_END);
    *size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    //读取文件内容
    char* buffer = (char*)malloc(*size);
    if (!buffer) {
        perror("malloc");
        fclose(fp);
        return NULL;
    }
    fread(buffer, 1, *size, fp);
    fclose(fp);
    return buffer;
}

//发送请求消息
//token: 获取的access token
//audio: 音频文件内容
//size: 音频文件大小
//return: 响应消息正文, NULL 表示失败
char* send_request(char* token, char* audio, size_t size)
{
    char* url = NULL;
    asprintf(&url, "http://vop.baidu.com/server_api?cuid=hqyj&token=%s", token);

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: audio/pcm; rate=16000");

    char* response = post(url, headers, audio, &size);

    free(url);
    curl_slist_free_all(headers);

    return response;
}

//处理服务器返回的响应消息
void process_response(char* response, size_t size)
{
    //解析 JSON 响应
    cJSON *json = cJSON_ParseWithLength(response, size);
    if (!json) {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return;
    }

    //判断err_no字段
    cJSON* err_no = cJSON_GetObjectItem(json, "err_no");
    if (!err_no) {
        fprintf(stderr, "err_no 字段不存在\n");
        cJSON_Delete(json);
        return;
    }
    //判断err_no的值
    if (err_no->valueint != 0) {
        //打印错误信息
        cJSON* err_msg = cJSON_GetObjectItem(json, "err_msg");
        if (err_msg)
        {
            fprintf(stderr, "err_msg: %s\n", err_msg->valuestring);
        }
        cJSON_Delete(json);
        return;
    }

    // 获取 "result" 字段中的第一个元素
    cJSON *result = cJSON_GetObjectItem(json, "result");
    if (!result) {
        fprintf(stderr, "JSON 格式错误: 找不到'result' 字段\n");
        cJSON_Delete(json);
        return;
    }

    if (cJSON_GetArraySize(result) > 0) {
        // 获取第一个元素的 "content" 字段
        cJSON *content = cJSON_GetArrayItem(result, 0);
        //打印结果
        printf("result: %s\n", content->valuestring);
    }

    cJSON_Delete(json);
}

int main()
{

    //读取音频文件
    size_t size;
    char* buffer = load_audio_file("output.pcm", &size);
    if (!buffer) {
        printf("打开文件失败");
        return EXIT_FAILURE;
    }

    //读取配置信息，API KEY 和 SECRET KEY
    cJSON *config = read_config("config.json");
    if (!config) {
        printf("config: %s\n", cJSON_Print(config));
        free(buffer);
        return EXIT_FAILURE;
    }
    cJSON* api_key = cJSON_GetObjectItem(config, "api_key");
    cJSON* secret_key = cJSON_GetObjectItem(config, "secret_key");
    if (!api_key ||!secret_key) {
        fprintf(stderr, "配置文件错误: 找不到 'api_key' 或'secret_key' 字段\n");
        cJSON_Delete(config);
        free(buffer);
        return EXIT_FAILURE;
    }

    //获取token
    char* token = get_access_token(api_key->valuestring, secret_key->valuestring);
    cJSON_Delete(config);
    if (!token) {
        fprintf(stderr, "获取 token 失败\n");
        free(buffer);
        return EXIT_FAILURE;
    }
    
    //调用百度语音识别 API
    char* response = send_request(token, buffer, size);
    free(buffer);
    free(token);
    if (!response) {
        fprintf(stderr, "调用百度语音识别 API 失败\n");
        return EXIT_FAILURE;
    }

    //处理服务器返回的响应消息
    process_response(response, size);

    free(response);

    return EXIT_SUCCESS;
}