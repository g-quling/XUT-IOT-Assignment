#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <cjson/cJSON.h>

// 打印当前IP地址的天气
void print_recommended_weather(const char *response, size_t size) {
    // 解析 JSON 响应
    cJSON *json = cJSON_ParseWithLength(response, size);
    if (json == NULL) {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return;
    }

    // 获取 "data" 对象
    cJSON *data = cJSON_GetObjectItemCaseSensitive(json, "data");
    if (!cJSON_IsObject(data)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'data' 对象\n");
        cJSON_Delete(json);
        return;
    }

    // 获取 "region" 字符串
    cJSON *region = cJSON_GetObjectItemCaseSensitive(data, "region");
    if (!cJSON_IsString(region) || (region->valuestring == NULL)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'region' 字符串\n");
        cJSON_Delete(json);
        return;
    }

    printf("region：%s\n", region->valuestring);

      // 获取 "waetherData" 字符串
    cJSON *weatherData = cJSON_GetObjectItemCaseSensitive(data, "weatherData");
    if (!cJSON_IsObject(weatherData)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'waeatherData' 字符串\n");
        cJSON_Delete(json);
        return;
    }

    // 要获取的键列表
    const char *keys[] = {
        "temperature", "windDirection", "windPower", "humidity",
        "updateTime", "weather", "visibility", "rainfall", "pm25"
    };
    const int num_keys = sizeof(keys) / sizeof(keys[0]);

    // 遍历键列表并获取相应的值
    for (int i = 0; i < num_keys; i++) {
        cJSON *item = cJSON_GetObjectItemCaseSensitive(weatherData, keys[i]);
        if (cJSON_IsString(item)) {
            printf("%s: %s\n", keys[i], item->valuestring);
        } else if (cJSON_IsNumber(item)) {
            printf("%s: %lf\n", keys[i], item->valuedouble);
        } else {
            fprintf(stderr, "JSON 格式错误: 找不到 '%s' 字符串或数值\n", keys[i]);
            cJSON_Delete(json);
            return;
        }
    }

    // 释放 JSON 对象
    cJSON_Delete(json);
}

int main(void) {
    CURL *client;
    CURLcode err;
    char *response;
    size_t size;

    // 创建内存流
    FILE *memstream = open_memstream(&response, &size);
    if (memstream == NULL) {
        perror("open_memstream");
        return 1;
    }

    // 初始化 CURL
    curl_global_init(CURL_GLOBAL_ALL);
    client = curl_easy_init();

    // 设置 CURL 选项
    curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/info");
    curl_easy_setopt(client, CURLOPT_WRITEDATA, memstream);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "X-User-Token:U5WjqcS8a1NFEA2mwPgAaTyE25/MBhDz"); // 替换 YOUR_API_KEY 为你的 API Key
    curl_easy_setopt(client, CURLOPT_HTTPHEADER, headers);

    // 执行 CURL 请求
    err = curl_easy_perform(client);
    fclose(memstream); // 关闭内存流

    if (err != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() 失败: %s\n", curl_easy_strerror(err));
    } else {
        // 打印推荐的古诗词
        print_recommended_weather(response, size);
    }

    // 清理资源
    curl_slist_free_all(headers);
    curl_easy_cleanup(client);
    free(response);
    curl_global_cleanup();

    return 0;
}
