#include <stdio.h>
#include <gpiod.h>
#include <stdbool.h>
#include <alsa/asoundlib.h>


int running =1;

int get_playback_volume(const char* card, const char* selem) {
    int err;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;

    // 打开混音器
    if ((err = snd_mixer_open(&handle, 0)) < 0) {
        fprintf(stderr, "Mixer %s open error: %s\n", card, snd_strerror(err));
        return err;
    }

    // 附加控制接口到混音器
    if ((err = snd_mixer_attach(handle, card)) < 0) {
        fprintf(stderr, "Mixer attach %s error: %s\n", card, snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 注册混音器
    if ((err = snd_mixer_selem_register(handle, NULL, NULL)) < 0) {
        fprintf(stderr, "Mixer register error: %s\n", snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 加载混音器元素
    if ((err = snd_mixer_load(handle)) < 0) {
        fprintf(stderr, "Mixer %s load error: %s\n", card, snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 分配简单元素ID
    snd_mixer_selem_id_alloca(&sid);

    // 设置简单元素的名称
    snd_mixer_selem_id_set_name(sid, selem);

    // 查找简单元素
    snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, sid);
    if (!elem) {
        fprintf(stderr, "Unable to find simple control '%s',%i\n", selem, 0);
        snd_mixer_close(handle);
        return -ENOENT;
    }

    // 获取回放通道音量
    long volume = 0;
    if ((err = snd_mixer_selem_get_playback_volume(elem, 0, &volume)) < 0) {
        fprintf(stderr, "Unable to get playback volume: %s\n", snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 关闭混音器
    snd_mixer_close(handle);

    return volume; // 成功返回音量
}

int set_playback_volume(const char* card, const char* selem, long volume)
{
    int err;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;

    // 打开混音器
    if ((err = snd_mixer_open(&handle, 0)) < 0) {
        fprintf(stderr, "Mixer %s open error: %s\n", card, snd_strerror(err));
        return err;
    }

    // 附加控制接口到混音器
    if ((err = snd_mixer_attach(handle, card)) < 0) {
        fprintf(stderr, "Mixer attach %s error: %s\n", card, snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 注册混音器
    if ((err = snd_mixer_selem_register(handle, NULL, NULL)) < 0) {
        fprintf(stderr, "Mixer register error: %s\n", snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 加载混音器元素
    if ((err = snd_mixer_load(handle)) < 0) {
        fprintf(stderr, "Mixer %s load error: %s\n", card, snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 分配简单元素ID
    snd_mixer_selem_id_alloca(&sid);

    // 设置简单元素的名称
    snd_mixer_selem_id_set_name(sid, selem);

    // 查找简单元素
    snd_mixer_elem_t *elem = snd_mixer_find_selem(handle, sid);
    if (!elem) {
        fprintf(stderr, "Unable to find simple control '%s',%i\n", selem, 0);
        snd_mixer_close(handle);
        return -ENOENT;
    }

    // 获取音量范围
    long min, max;
    if ((err = snd_mixer_selem_get_playback_volume_range(elem, &min, &max)) < 0)
    {
        fprintf(stderr, "Unable to get playback volume range: %s\n", snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    if (volume < min)
    {
        volume = min;
    }

    if (volume > max)
    {
        volume = max;
    }

    // 设置回放通道音量
    if ((err = snd_mixer_selem_set_playback_volume_all(elem, volume)) < 0) {
        fprintf(stderr, "Unable to set playback volume: %s\n", snd_strerror(err));
        snd_mixer_close(handle);
        return err;
    }

    // 关闭混音器
    snd_mixer_close(handle);

    return volume; // 成功返回设置后音量
}

void signal_handler(int signum) {
    running = 0;
}

int main() 
{
    // 初始化 libgpiod
    struct gpiod_chip *chip;
    struct gpiod_line *k2_line, *k3_line;
    int last_value2 = 1;
    int last_value3 = 1;

    chip = gpiod_chip_open_by_label("GPIOF");
    if (!chip) {
        perror("打开GPIO芯片失败");
        return 1;
        // 错误处理
    }

     // 获取GPIO线
   
    k2_line = gpiod_chip_get_line(chip, 7);  // PF7
    if (!k2_line) {
        perror("获取GPIO线失败");
        gpiod_chip_close(chip);
        return 1;}
    k3_line = gpiod_chip_get_line(chip, 8);  // PF8
     if (!k3_line) {
        perror("获取GPIO线失败");
        gpiod_chip_close(chip);
        return 1;}

    // 设置按键为输入
    gpiod_line_request_input(k2_line, "k2");
    gpiod_line_request_input(k3_line, "k3");

    // 设置音量控制的卡和元素
    const char *audio_card = "default";
    const char *audio_elem = "PCM";

    // 循环检测按键状态
    while (running) {
        // 检测 k2 和 k3 的状态
        int k2_state = gpiod_line_get_value(k2_line);
        int k3_state = gpiod_line_get_value(k3_line);

        if (k2_state==0 && k2_state!=last_value2 ) {
            // 按下 k2，增加音量
            // 首先获取当前音量
            long current_volume = get_playback_volume(audio_card, audio_elem);
            printf("Past vloume=%d\n",current_volume);
            set_playback_volume(audio_card, audio_elem, current_volume + 5);
            printf("key2 pressed\n");
            long current_volume2 = get_playback_volume(audio_card, audio_elem);
            printf("Now vloume=%d\n",current_volume2);
        } else if (k3_state == 0 && k3_state!=last_value3) {
            // 按下 k3，减小音量
            // 首先获取当前音量
            long current_volume = get_playback_volume(audio_card, audio_elem);
            printf("Past vloume=%d\n",current_volume);
            set_playback_volume(audio_card, audio_elem, current_volume - 5);
            long current_volume2 = get_playback_volume(audio_card, audio_elem);
            printf("key3 pressed\n");
            printf("Now vloume=%d\n",current_volume2);
        }
        last_value2 = k2_state ;
        last_value3 = k3_state ;
         usleep(100000); // 100ms
        }
        
        // 等待一段时间后再次检测按键状态
       
    

    // 释放资源
    gpiod_line_release(k2_line);
    gpiod_line_release(k3_line);
    gpiod_chip_close(chip);

    return 0;
}
