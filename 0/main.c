#include <stdio.h>
#include <stdbool.h>

/* 1990年1月1日 是星期一 */

//返回从1月1日到输入月份1日之间的天数
int get_monthdays(int month, bool leap)
{
    int days = 0;
    int monthdays[] = {0, 31, 28 + !!leap, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    for (int i = 0; i < month; i++)
    {
        days += monthdays[i];
    }

    return days;
}

bool is_valid(int year, int month, int day)
{
    if (year < 1990)
    {
        return false;
    }

    if (month < 1 || month > 12)
    {
        return false;
    }

    return true;
}

bool is_leap(int year)
{
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

int main()
{
    int year, month, day, week;
    int sum = 0;
    int i;

    scanf("%d/%d/%d", &year, &month, &day);
    if (is_valid(year, month, day) == false)
    {
        printf("输入日期错误\n");
        return 1;
    }

    for (i = 1990; i < year; i++)
    {
        if (is_leap(i))
        {
            sum += 366;
        }
        else
        {
            sum += 365;
        }
    }

    sum += get_monthdays(month, is_leap(year));
    sum += day;
    week = sum % 7;

    char* weekday[] = {"日", "一", "二", "三", "四", "五", "六"};
    printf("%d年%d月%d日是星期%s\n", year, month, day, weekday[week]);
    return 0;
}