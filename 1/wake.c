#include "snowboy/snowboy-detect-c-wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "record.h"

#define SILENCE_THRESHOLD 5  // 静音阈值秒数

int main()
{
    // 创建 Snowboy 检测器
    SnowboyDetect* detector = SnowboyDetectConstructor("common.res", "model.pmdl");
    if (!detector)
    {
        return EXIT_FAILURE;  // 如果检测器创建失败，退出程序
    }

    // 设置 Snowboy 的检测灵敏度
    SnowboyDetectSetSensitivity(detector, "0.5");
    
    // 获取检测器支持的音频数据参数
    int bits = SnowboyDetectBitsPerSample(detector);  // 采样深度
    int channels = SnowboyDetectNumChannels(detector);  // 声道数量
    int rate = SnowboyDetectSampleRate(detector);  // 采样频率

    // 打印音频数据参数
    printf("采样深度: %d\n", bits);
    printf("声道数量: %d\n", channels);
    printf("采样频率: %d\n", rate);

    // 打开音频采集设备
    snd_pcm_uframes_t period = 999;
    snd_pcm_t* capture = record_open("hw:0,0", SND_PCM_FORMAT_S16_LE, channels, rate, &period);
    if (!capture)
    {
        return EXIT_FAILURE;  // 如果音频采集设备打开失败，退出程序
    }

    // 分配缓冲区用于存储音频数据
    char* buffer = malloc(snd_pcm_frames_to_bytes(capture, period));
    if (!buffer) {
        perror("malloc");
        record_close(capture);  // 关闭音频采集设备
        SnowboyDetectDestructor(detector);  // 销毁 Snowboy 检测器
        return EXIT_FAILURE;  // 如果分配缓冲区失败，退出程序
    }

    FILE* outputFile = NULL;  // 用于保存录音文件的文件指针
    time_t silenceStartTime = 0;  // 静音开始时间
    int recording = 0;  // 录音状态标志

    while (1)
    {
        // 从 PCM 设备读取数据
        snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period);
        if (frames < 0)
        {
            fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
            snd_pcm_recover(capture, frames, 0);  // 尝试恢复 PCM 设备
            continue;
        }

        // 运行 Snowboy 检测
        int status = SnowboyDetectRunDetection(detector, (int16_t*)buffer, snd_pcm_frames_to_bytes(capture, frames) / sizeof(int16_t), 0);
        
        if (status > 0)
        {
            // 检测到唤醒词
            if (!recording) {
                printf("检测到唤醒词\n");
                recording = 1;  // 设置录音状态
                silenceStartTime = 0;  // 重置静音开始时间
                // 打开文件以保存录音数据
                outputFile = fopen("output.pcm", "wb");
                if (!outputFile) {
                    perror("fopen");
                    free(buffer);  // 释放缓冲区
                    record_close(capture);  // 关闭音频采集设备
                    SnowboyDetectDestructor(detector);  // 销毁 Snowboy 检测器
                    return EXIT_FAILURE;  // 如果打开文件失败，退出程序
                }
            }
        }

        if (recording) {
            // 将音频数据写入文件
            fwrite(buffer, 1, snd_pcm_frames_to_bytes(capture, frames), outputFile);

            // 检测静音状态
            if (status == -2) {
                if (silenceStartTime == 0) {
                    silenceStartTime = time(NULL);  // 记录静音开始时间
                } else if (time(NULL) - silenceStartTime >= SILENCE_THRESHOLD) {
                    // 如果静音持续时间超过阈值，停止录音
                    printf("检测到连续%d秒静音，停止录音\n", SILENCE_THRESHOLD);
                    fclose(outputFile);  // 关闭文件
                    outputFile = NULL;
                    recording = 0;  // 重置录音状态
                    silenceStartTime = 0;  // 重置静音开始时间
                }
            } else {
                silenceStartTime = 0;  // 如果有声音，则重置静音开始时间
            }
        }
    }

    free(buffer);  // 释放缓冲区
    record_close(capture);  // 关闭音频采集设备
    SnowboyDetectDestructor(detector);  // 销毁 Snowboy 检测器

    return EXIT_SUCCESS;  // 正常退出程序
}
