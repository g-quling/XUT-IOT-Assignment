#include <gpiod.h> // 包含libgpiod库的头文件
#include <stdio.h> // 包含标准输入输出库
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#define GPIO_LINE 9

int main(void) {
    struct gpiod_chip *chip;
    struct gpiod_line *line;
    int value, last_value;
    pid_t record_pid = -1;

    // 打开GPIO芯片
    chip = gpiod_chip_open_by_label("GPIOF");
    if (!chip) {
        perror("打开GPIO芯片失败");
        return 1;
    }

    // 获取GPIO线
    line = gpiod_chip_get_line(chip, GPIO_LINE);
    if (!line) {
        perror("获取GPIO线失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 将GPIO线设置为输入模式
    if (gpiod_line_request_input(line, "key1")) {
        perror("请求将GPIO线设置为输入模式失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 获取初始的GPIO线值
    last_value = gpiod_line_get_value(line);

    // 无限循环检测GPIO线值的变化
    while (1) {
        // 获取当前的GPIO线值
        value = gpiod_line_get_value(line);

        // 如果当前值与上一次的值不同，说明按键状态发生了变化
        if (value != last_value) {
            // 如果当前值为0，表示按键被按下
            if (value == 0) {
                printf("key pressed\n");
                // 启动record进程
                record_pid = fork();
                if (record_pid == 0) {
                    execl("./record", "record", (char *)NULL);
                    perror("启动record进程失败");
                    exit(1);
                }
            } 
            // 如果当前值为1，表示按键被释放
            else {
                printf("key released\n");
                // 停止record进程
                if (record_pid > 0) {
                    kill(record_pid, SIGTERM);
                    waitpid(record_pid, NULL, 0);
                    record_pid = -1;
                }
            }
            // 更新上一次的值为当前值
            last_value = value;
        }
        // 延时100毫秒，防止检测过于频繁
        usleep(100000);
    }

    // 关闭GPIO芯片
    gpiod_chip_close(chip);
    return 0;
}