#include "snowboy/snowboy-detect-c-wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include "record.h"

#define SILENCE_THRESHOLD 5000 // 5秒静音阈值，单位为毫秒

int main()
{
    // 创建Snowboy检测器
    SnowboyDetect* detector = SnowboyDetectConstructor("common.res", "computer.umdl");
    if (!detector)
    {
        fprintf(stderr, "Failed to create Snowboy detector\n");
        return EXIT_FAILURE;
    }

    // 设置灵敏度和音频增益
    SnowboyDetectSetSensitivity(detector, "0.6");
    SnowboyDetectSetAudioGain(detector, 1.0);

    // 获取检测器支持的音频数据参数
    int bits = SnowboyDetectBitsPerSample(detector);
    int channels = SnowboyDetectNumChannels(detector);
    int rate = SnowboyDetectSampleRate(detector);

    printf("采样深度: %d\n", bits);
    printf("声道数量: %d\n", channels);
    printf("采样频率: %d\n", rate);

    // 打开音频采集设备
    snd_pcm_uframes_t period = 999;
    snd_pcm_t* capture = record_open("hw:0,1", SND_PCM_FORMAT_S16_LE, channels, rate, &period);
    if (!capture)
    {
        fprintf(stderr, "Failed to open PCM device\n");
        SnowboyDetectDestructor(detector);
        return EXIT_FAILURE;
    }

    char* buffer = malloc(snd_pcm_frames_to_bytes(capture, period)); // 分配缓冲区
    if (!buffer) {
        perror("malloc");
        record_close(capture);
        SnowboyDetectDestructor(detector);
        return EXIT_FAILURE;
    }

    bool recording = false;
    int silent_frames = 0;
    FILE* fp = NULL;

    while (1)
    {
        snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
        if (frames < 0)
        {
            fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
            snd_pcm_recover(capture, frames, 0);
        }

        int status = SnowboyDetectRunDetection(detector, (int16_t*)buffer, snd_pcm_frames_to_bytes(capture, frames) / sizeof(int16_t), 0);
        if (status > 0)
        {
            printf("检测到唤醒词\n");
            recording = true;
            silent_frames = 0; // 重置静音帧计数
            fp = fopen("output.wav", "wb"); // 打开文件以写入音频数据
            if (!fp) {
                perror("Failed to open file for recording");
                break;
            }
        }
        else if (status == -2)
        {
            silent_frames += frames;
            if (recording && silent_frames >= (SILENCE_THRESHOLD * rate / 1000))
            {
                printf("静音超过5秒，结束录音\n");
                recording = false;
                if (fp) {
                    fclose(fp);
                    fp = NULL;
                }
            }
        }

        if (recording && fp)
        {
            fwrite(buffer, snd_pcm_frames_to_bytes(capture, frames), 1, fp);
        }
    }

    free(buffer);
    if (fp) {
        fclose(fp);
    }
    record_close(capture);
    SnowboyDetectDestructor(detector);

    return EXIT_SUCCESS;
}
             