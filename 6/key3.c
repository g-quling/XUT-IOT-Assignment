#include <gpiod.h> // 包含libgpiod库的头文件
#include <stdio.h> // 包含标准输入输出库
#include <unistd.h> // 包含UNIX标准库，用于usleep函数
#include <stdlib.h> // 包含标准库，用于system函数

#define GPIO_LINE 8 // 定义GPIO线的编号，这里是PF8

void decrease_volume() {
    // 使用正确的控件名称减小PCM音量
    if (system("amixer set PCM 5%-")) {
        perror("无法减小音量");
    } else {
        printf("volume decreased\n");
    }
}

int main(void) {
    struct gpiod_chip *chip; // 定义指向GPIO芯片的指针
    struct gpiod_line *line; // 定义指向GPIO线的指针
    int value, last_value; // 定义当前值和上一次的值，用于检测状态变化

    // 打开GPIO芯片
    chip = gpiod_chip_open_by_label("GPIOF");
    if (!chip) {
        perror("打开GPIO芯片失败");
        return 1;
    }

    // 获取GPIO线
    line = gpiod_chip_get_line(chip, GPIO_LINE);
    if (!line) {
        perror("获取GPIO线失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 将GPIO线设置为输入模式
    if (gpiod_line_request_input(line, "key3")) {
        perror("请求将GPIO线设置为输入模式失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 获取初始的GPIO线值
    last_value = gpiod_line_get_value(line);

    // 无限循环检测GPIO线值的变化
    while (1) {
        // 获取当前的GPIO线值
        value = gpiod_line_get_value(line);
        
        // 如果当前值与上一次的值不同，说明按键状态发生了变化
        if (value != last_value) {
            // 如果当前值为0，表示按键被按下
            if (value == 0) {
                decrease_volume();
            }
            // 更新上一次的值为当前值
            last_value = value;
        }
        // 延时100毫秒，防止检测过于频繁
        usleep(100000);
    }

    // 关闭GPIO芯片
    gpiod_chip_close(chip);
    return 0;
}
