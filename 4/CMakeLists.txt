cmake_minimum_required(VERSION 3.0.0)
project(voice_assistant VERSION 0.1.0 LANGUAGES C)

add_library(record STATIC record.c)
add_library(control STATIC control.c)

add_executable(voice_assistant main.c)


add_executable(play play.c)
target_link_libraries(play asound)

add_executable(key key.c)
target_link_libraries(key gpiod record asound control)