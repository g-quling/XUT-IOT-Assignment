#ifndef RECORD_H
#define RECORD_H

#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <errno.h>
#include <signal.h>

extern snd_pcm_t *capture;
extern char *buffer;
extern FILE *pcm_file;
extern volatile sig_atomic_t keep_recording;

void handle_sigterm(int sig);
snd_pcm_t* record_start(const char* name, snd_pcm_format_t format, unsigned int channel, unsigned int rate, snd_pcm_uframes_t* period);
void start_recording(const char *pcm_device, const char *output_file, snd_pcm_format_t format, unsigned int channels, unsigned int rate);
void stop_recording();

#endif // RECORD_H
