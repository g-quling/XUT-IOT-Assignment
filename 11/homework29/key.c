#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gpiod.h>
#include <alsa/asoundlib.h>
#include "record.h"
#include "control.h"

#define GPIO_LINE_K1 9   // K1按钮的GPIO线编号
#define GPIO_LINE_K2 7   // K2按钮的GPIO线编号
#define GPIO_LINE_K3 8   // K3按钮的GPIO线编号

// 主函数
int main(void) {
    struct gpiod_chip *chip;      // 指向GPIO芯片的指针
    struct gpiod_line *line_k1;   // 指向K1按钮GPIO线的指针
    struct gpiod_line *line_k2;   // 指向K2按钮GPIO线的指针
    struct gpiod_line *line_k3;   // 指向K3按钮GPIO线的指针
    int value_k1, last_value_k1;  // K1按钮的当前值和上一次的值
    int value_k2, last_value_k2;  // K2按钮的当前值和上一次的值
    int value_k3, last_value_k3;  // K3按钮的当前值和上一次的值
    long volume;                  // 当前音量值
    snd_pcm_t *capture = NULL;    // PCM设备句柄，用于音频录制
    snd_pcm_uframes_t period;     // 每个周期的帧数
    char *buffer;                 // 缓冲区，用于存储录制的音频数据
    FILE *pcm_file;               // 输出PCM文件的指针

    // 打开GPIO芯片
    chip = gpiod_chip_open_by_label("GPIOF");
    if (!chip) {
        perror("打开GPIO芯片失败");
        return 1;
    }

    // 获取K1按钮的GPIO线
    line_k1 = gpiod_chip_get_line(chip, GPIO_LINE_K1);
    if (!line_k1) {
        perror("获取K1按钮GPIO线失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 获取K2按钮的GPIO线
    line_k2 = gpiod_chip_get_line(chip, GPIO_LINE_K2);
    if (!line_k2) {
        perror("获取K2按钮GPIO线失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 获取K3按钮的GPIO线
    line_k3 = gpiod_chip_get_line(chip, GPIO_LINE_K3);
    if (!line_k3) {
        perror("获取K3按钮GPIO线失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 将K1按钮GPIO线设置为输入模式
    if (gpiod_line_request_input(line_k1, "key_k1")) {
        perror("请求K1按钮GPIO线输入模式失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 将K2按钮GPIO线设置为输入模式
    if (gpiod_line_request_input(line_k2, "key_k2")) {
        perror("请求K2按钮GPIO线输入模式失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 将K3按钮GPIO线设置为输入模式
    if (gpiod_line_request_input(line_k3, "key_k3")) {
        perror("请求K3按钮GPIO线输入模式失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 获取初始按钮状态
    last_value_k1 = gpiod_line_get_value(line_k1);
    last_value_k2 = gpiod_line_get_value(line_k2);
    last_value_k3 = gpiod_line_get_value(line_k3);

    // 获取当前音量
    volume = get_playback_volume("hw:0", "Analog");

    // 无限循环检测按钮状态变化
    while (1) {
        // 获取K1按钮的当前状态
        value_k1 = gpiod_line_get_value(line_k1);
        // 获取K2按钮的当前状态
        value_k2 = gpiod_line_get_value(line_k2);
        // 获取K3按钮的当前状态
        value_k3 = gpiod_line_get_value(line_k3);

        // 检测K1按钮状态变化
        if (value_k1 != last_value_k1) {
            // 按下K1按钮
            if (value_k1 == 0 && capture == NULL) {
                printf("K1 pressed - 开始录音\n");
                capture = record_start("hw:0,1", SND_PCM_FORMAT_S16_LE, 2, 44100, &period);
                if (!capture) {
                    perror("录音启动失败");
                    gpiod_chip_close(chip);
                    return 1;
                }
                // 分配缓冲区用于存储音频数据
                buffer = (char *)malloc(snd_pcm_frames_to_bytes(capture, period));
                if (!buffer) {
                    perror("分配缓冲区失败");
                    record_stop(capture);
                    gpiod_chip_close(chip);
                    return 1;
                }
                // 打开输出PCM文件
                pcm_file = fopen("output.pcm", "wb");
                if (!pcm_file) {
                    perror("打开输出文件失败");
                    free(buffer);
                    record_stop(capture);
                    gpiod_chip_close(chip);
                    return 1;
                }
                printf("录音中...\n");
            } 
            // 抬起K1按钮
            else if (value_k1 == 1 && capture != NULL) {
                printf("K1 released - 停止录音\n");
                free(buffer);         // 释放缓冲区
                fclose(pcm_file);     // 关闭输出文件
                record_stop(capture); // 停止音频采集
                capture = NULL;       // 重置capture指针
                printf("录音停止。\n");
            }
            // 更新K1按钮状态
            last_value_k1 = value_k1;
        }

        // 检测K2按钮状态变化
        if (value_k2 != last_value_k2) {
            // 按下K2按钮
            if (value_k2 == 0) {
                printf("K2 pressed - 增加音量\n");
                volume += 5; // 增加音量
                volume = set_playback_volume("hw:0", "Analog", volume);
                printf("新音量: %ld\n", volume);
            }
            // 更新K2按钮状态
            last_value_k2 = value_k2;
        }

        // 检测K3按钮状态变化
        if (value_k3 != last_value_k3) {
            // 按下K3按钮
            if (value_k3 == 0) {
                printf("K3 pressed - 减少音量\n");
                volume -= 5; // 减少音量
                volume = set_playback_volume("hw:0", "Analog", volume);
                printf("新音量: %ld\n", volume);
            }
            // 更新K3按钮状态
            last_value_k3 = value_k3;
        }

        // 如果正在录音，继续读取音频数据
        if (capture != NULL) {
            snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period);
            if (frames < 0) {
                fprintf(stderr, "读取音频数据出错: %s\n", snd_strerror(frames));
                snd_pcm_recover(capture, frames, 0); // 尝试恢复PCM设备
            } else {
                // 将读取的数据写入输出文件
                fwrite(buffer, snd_pcm_frames_to_bytes(capture, frames), 1, pcm_file);
            }
        }

        // 延时100毫秒，防止检测过于频繁
       // usleep(100000);
    }

    // 释放GPIO线资源
    gpiod_line_release(line_k1);
    gpiod_line_release(line_k2);
    gpiod_line_release(line_k3);

    // 关闭GPIO芯片
    gpiod_chip_close(chip);

    return 0;
}
