#include <stdio.h>
#include <stdbool.h>
/* 1990年1月1日 是星期一 */

bool is_valid(int year,int month,int day)
{
    if(year<1990)
        return false;
    if(month<1||month>12)
        return false;
}
//判断润年
bool year_is_leap(int i)
{
    return(i % 4 == 0 && i % 100 != 0) || i % 400 == 0;
}
//返回從1月1日到輸入月份之間的天數
int get_monthdays(int mounth,bool leap)
{
    int days=0;
    int a[]={31,28+!!leap,31,30,31,30,31,31,30,31,30,31};
    for(int i=0;i<mounth-1;i++)
        days+=a[i];
    return days;
}

int main()
{
    int year, month, day, week;
    int sum = 0;
    int i;

    scanf("%d/%d/%d", &year, &month, &day);
    if(!is_valid(year,month,day))
    {
        printf("invalid data\n");
        return 1;
    }
    for (i = 1990; i < year; i++)
    {
        if(year_is_leap(i))
            sum+=366;
        else
            sum+=365;
    }

    sum = get_monthdays(month,year_is_leap(year));
    sum += day;
    week = sum % 7;
    
    char* weekday[]={"日","一","二","三","四","五","六"};
    
    printf("%d年%d月%d日是星期%s\n", year, month, day, weekday[week]);

    return 0;
}