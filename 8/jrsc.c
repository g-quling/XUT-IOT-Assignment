#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <cjson/cJSON.h>

// 打印推荐的古诗词
void print_recommended_poetry(const char *response, size_t size) {
    // 解析 JSON 响应
    cJSON *json = cJSON_ParseWithLength(response, size);
    if (json == NULL) {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return;
    }

    // 获取 "data" 对象
    cJSON *data = cJSON_GetObjectItemCaseSensitive(json, "data");
    if (!cJSON_IsObject(data)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'data' 对象\n");
        cJSON_Delete(json);
        return;
    }

    // 获取 "content" 字符串
    cJSON *content = cJSON_GetObjectItemCaseSensitive(data, "content");
    if (!cJSON_IsString(content) || (content->valuestring == NULL)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'content' 字符串\n");
        cJSON_Delete(json);
        return;
    }

    // 打印推荐的古诗词
    printf("推荐的古诗词：%s\n", content->valuestring);

    // 释放 JSON 对象
    cJSON_Delete(json);
}

// 打印今天的天气信息
void print_today_weather(const char *response, size_t size) {
    // 解析 JSON 响应
    cJSON *json = cJSON_ParseWithLength(response, size);
    if (json == NULL) {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return;
    }

    // 获取 "data" 对象
    cJSON *data = cJSON_GetObjectItemCaseSensitive(json, "data");
    if (!cJSON_IsObject(data)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'data' 对象\n");
        cJSON_Delete(json);
        return;
    }

    // 获取 "region" 字符串
    cJSON *region = cJSON_GetObjectItemCaseSensitive(data, "region");
    if (!cJSON_IsString(region) || (region->valuestring == NULL)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'region' 字符串\n");
        cJSON_Delete(json);
        return;
    }

    // 获取 "weatherData" 对象
    cJSON *weatherData = cJSON_GetObjectItemCaseSensitive(data, "weatherData");
    if (!cJSON_IsObject(weatherData)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'weatherData' 对象\n");
        cJSON_Delete(json);
        return;
    }

    // 逐项获取并打印天气信息
    cJSON *temperature = cJSON_GetObjectItemCaseSensitive(weatherData, "temperature");
    cJSON *windDirection = cJSON_GetObjectItemCaseSensitive(weatherData, "windDirection");
    cJSON *windPower = cJSON_GetObjectItemCaseSensitive(weatherData, "windPower");
    cJSON *humidity = cJSON_GetObjectItemCaseSensitive(weatherData, "humidity");
    cJSON *updateTime = cJSON_GetObjectItemCaseSensitive(weatherData, "updateTime");
    cJSON *weather = cJSON_GetObjectItemCaseSensitive(weatherData, "weather");
    cJSON *visibility = cJSON_GetObjectItemCaseSensitive(weatherData, "visibility");
    cJSON *rainfall = cJSON_GetObjectItemCaseSensitive(weatherData, "rainfall");
    cJSON *pm25 = cJSON_GetObjectItemCaseSensitive(weatherData, "pm25");

    if (!cJSON_IsNumber(temperature) ||
        !cJSON_IsString(windDirection) ||
        !cJSON_IsNumber(windPower) ||
        !cJSON_IsNumber(humidity) ||
        !cJSON_IsString(updateTime) ||
        !cJSON_IsString(weather) ||
        !cJSON_IsString(visibility) ||
        !cJSON_IsNumber(rainfall) ||
        !cJSON_IsNumber(pm25)) {
        fprintf(stderr, "JSON 格式错误: 天气信息字段类型不正确\n");
        cJSON_Delete(json);
        return;
    }

    printf("地区：%s\n", region->valuestring);
    printf("今天的天气信息：\n");
    printf("温度：%d°C\n", temperature->valueint);
    printf("风向：%s\n", windDirection->valuestring);
    printf("风力：%d级\n", windPower->valueint);
    printf("湿度：%d%%\n", humidity->valueint);
    printf("更新时间：%s\n", updateTime->valuestring);
    printf("天气：%s\n", weather->valuestring);
    printf("能见度：%s\n", visibility->valuestring);
    printf("降水量：%dmm\n", rainfall->valueint);
    printf("PM2.5：%d\n", pm25->valueint);

    // 释放 JSON 对象
    cJSON_Delete(json);
}



int main(void) {
    CURL *client;
    CURLcode err;
    char *response;
    size_t size;

    // 创建内存流
    FILE *memstream = open_memstream(&response, &size);
    if (memstream == NULL) {
        perror("open_memstream");
        return 1;
    }

    // 初始化 CURL
    curl_global_init(CURL_GLOBAL_ALL);
    client = curl_easy_init();

    // 设置 CURL 选项
    curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/sentence");
    curl_easy_setopt(client, CURLOPT_WRITEDATA, memstream);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "X-User-Token: o9M10gtneoIxRLk5UO13fUG7guYxnS7h"); // 替换 YOUR_API_KEY 为你的 API Key
    curl_easy_setopt(client, CURLOPT_HTTPHEADER, headers);

    // 执行 CURL 请求
    err = curl_easy_perform(client);
    fclose(memstream); // 关闭内存流

    if (err != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() 失败: %s\n", curl_easy_strerror(err));
    } else {
        // 打印推荐的古诗词
        print_recommended_poetry(response, size);
    }

    // 重新创建内存流以调用天气接口
    memstream = open_memstream(&response, &size);
    if (memstream == NULL) {
        perror("open_memstream");
        return 1;
    }

    // 设置 CURL 选项
    curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/info");
    curl_easy_setopt(client, CURLOPT_WRITEDATA, memstream);

    // 执行 CURL 请求
    err = curl_easy_perform(client);
    fclose(memstream); // 关闭内存流

    if (err != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() 失败: %s\n", curl_easy_strerror(err));
    } else {
        // 打印今天的天气信息
        print_today_weather(response, size);
    }

    // 清理资源
    curl_slist_free_all(headers);
    curl_easy_cleanup(client);
    free(response);
    curl_global_cleanup();

    return 0;
}
