#include "snowboy/snowboy-detect-c-wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include "record.h"

int main()
{
    //创建snowboy检测器
    SnowboyDetect* detector = SnowboyDetectConstructor("common.res", "lymm.pmdl");
    if (!detector)
    {
        return EXIT_FAILURE;
    }

    //获取检测器支持的音频数据参数
    //采样深度
    int bits = SnowboyDetectBitsPerSample(detector);
    //声道数量
    int channels = SnowboyDetectNumChannels(detector);
    //采样频率
    int rate = SnowboyDetectSampleRate(detector);

    printf("采样深度: %d\n", bits);
    printf("声道数量: %d\n", channels);
    printf("采样频率: %d\n", rate);

    //打开音频采集设备
    snd_pcm_uframes_t period = 999;
    snd_pcm_t* capture = record_open("hw:0,1", SND_PCM_FORMAT_S16_LE, channels, rate, &period);
    if (!capture)
    {
        return EXIT_FAILURE;
    }

    char* buffer = malloc(snd_pcm_frames_to_bytes(capture, period)); // 分配缓冲区
    if (!buffer) {
        perror("malloc");
        record_close(capture);
        SnowboyDetectDestructor(detector);
        return EXIT_FAILURE;
    }

    int recording = 0;
    //检测到连续的静音次数
    int silence = 0;

    while (1)
    {
        snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
        if (frames < 0)
        {
            fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
            snd_pcm_recover(capture, frames, 0);
            continue;
        }

        //-2: 静音
        //-1: 检测出错
        // 0: 有声音，但不是唤醒词
        //>0: 检测到唤醒词
        int status = SnowboyDetectRunDetection(detector, (int16_t*)buffer, snd_pcm_frames_to_bytes(capture, frames) / sizeof(int16_t), 0);
        if (status > 0)
        {
            printf("检测到唤醒词，开始录音\n");
            recording = 1;
        }

        if (recording)
        {
            if (status == -2)
            {
                silence++;
            }

            if (status == 0)
            {
                silence = 0;
            }

            if (silence > 16)
            {
                printf("停止录音\n");
                recording = 0;
                silence = 0;
            }
        }
    }

    free(buffer);
    record_close(capture);
    SnowboyDetectDestructor(detector);

    return EXIT_SUCCESS;
}