cmake_minimum_required(VERSION 3.0.0)
project(voice-assistant VERSION 0.1.0 LANGUAGES C)

add_executable(voice-assistant main.c)

add_executable(control control.c)
target_link_libraries(control asound)

#add_executable(record record.c)
#target_link_libraries(record asound)

add_executable(play play.c)
target_link_libraries(play asound)

add_executable(key key.c record.c)
target_link_libraries(key gpiod asound)

add_subdirectory(snowboy)
add_executable(wake wake.c record.c)
target_link_libraries(wake snowboy-wrapper snowboy-detect cblas m stdc++ asound)

add_executable(jrsc jrsc.c)
target_link_libraries(jrsc curl cjson)
