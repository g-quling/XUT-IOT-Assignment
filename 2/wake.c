#include "snowboy/snowboy-detect-c-wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "record.h"

#define SILENCE_THRESHOLD 5 // 静音持续时间阈值（秒）

int main()
{
    // 创建snowboy检测器
    SnowboyDetect* detector = SnowboyDetectConstructor("common.res", "model.pmdl");
    if (!detector)
    {
        return EXIT_FAILURE;
    }

    // 获取检测器支持的音频数据参数
    int bits = SnowboyDetectBitsPerSample(detector);
    int channels = SnowboyDetectNumChannels(detector);
    int rate = SnowboyDetectSampleRate(detector);

    printf("采样深度: %d\n", bits);
    printf("声道数量: %d\n", channels);
    printf("采样频率: %d\n", rate);

    // 打开音频采集设备
    snd_pcm_uframes_t period = 999;
    snd_pcm_t* capture = record_open("hw:0,1", SND_PCM_FORMAT_S16_LE, channels, rate, &period);
    if (!capture)
    {
        return EXIT_FAILURE;
    }

    char* buffer = malloc(snd_pcm_frames_to_bytes(capture, period)); // 分配缓冲区
    if (!buffer) {
        perror("malloc");
        record_close(capture);
        SnowboyDetectDestructor(detector);
        return EXIT_FAILURE;
    }

    int recording = 0; // 录音状态标志
    time_t silence_start = 0; // 静音开始时间
    FILE* pcm_file = NULL; // 输出PCM文件

    while (1)
    {
        snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
        if (frames < 0)
        {
            fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
            snd_pcm_recover(capture, frames, 0);
            continue;
        }

        int status = SnowboyDetectRunDetection(detector, (int16_t*)buffer, snd_pcm_frames_to_bytes(capture, frames) / sizeof(int16_t), 0);

        if (status > 0) // 检测到唤醒词
        {
            if (!recording)
            {
                printf("检测到唤醒词，开始录音\n");
                recording = 1; // 开始录音
                silence_start = 0; // 重置静音计时
                pcm_file = fopen("output.pcm", "wb"); // 打开输出文件
                if (!pcm_file)
                {
                    perror("Error opening output file");
                    free(buffer);
                    record_close(capture);
                    SnowboyDetectDestructor(detector);
                    return EXIT_FAILURE;
                }
            }
        }

        if (recording)
        {
            if (status == -2) // 静音
            {
                if (silence_start == 0)
                {
                    silence_start = time(NULL); // 记录静音开始时间
                }
                else if (time(NULL) - silence_start >= SILENCE_THRESHOLD)
                {
                    printf("静音超过5秒，停止录音\n");
                    recording = 0; // 停止录音
                    silence_start = 0; // 重置静音计时
                    fclose(pcm_file); // 关闭文件
                    pcm_file = NULL;
                }
            }
            else // 有声音或唤醒词
            {
                silence_start = 0; // 重置静音计时
            }

            if (pcm_file)
            {
                fwrite(buffer, snd_pcm_frames_to_bytes(capture, frames), 1, pcm_file); // 将读取的数据写入文件
            }
        }
    }

    free(buffer);
    record_close(capture);
    SnowboyDetectDestructor(detector);

    return EXIT_SUCCESS;
}
