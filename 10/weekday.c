#include <stdio.h>

/* 1990年1月1日 是星期一 */

// 判断是否是闰年
int isLeapYear(int year) {
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
}

// 获取某个月的天数（考虑闰年）
int daysOfMonth(int month, int year) {
    int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if (month == 2 && isLeapYear(year)) return 29;
    return days[month - 1];
}

// 获取从1990年1月1日到指定日期的总天数
int daysFrom1990(int year, int month, int day) {
    int sum = 0;
    for (int i = 1990; i < year; i++) {
        sum += isLeapYear(i) ? 366 : 365;
    }
    for (int i = 1; i < month; i++) {
        sum += daysOfMonth(i, year);
    }
    sum += day;
    return sum;
}

int main() {
    int year, month, day, week;

    scanf("%d/%d/%d", &year, &month, &day);

    int totalDays = daysFrom1990(year, month, day);
    week = totalDays % 7;

    if (week == 0) {
        printf("%d年%d月%d日是星期日\n", year, month, day);
    } else {
        printf("%d年%d月%d日是星期%d\n", year, month, day, week);
    }

    return 0;
}
