#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <cjson/cJSON.h>

// 解析 JSON 并获取 data 对象
cJSON* get_data_object(const char *json_response, size_t json_size) {
    cJSON *json = cJSON_ParseWithLength(json_response, json_size);
    if (json == NULL) {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }

    cJSON *data = cJSON_GetObjectItemCaseSensitive(json, "data");
    if (!cJSON_IsObject(data)) {
        fprintf(stderr, "JSON 格式错误: 找不到 'data' 对象\n");
        cJSON_Delete(json);
        return NULL;
    }

    return data;
}

// 打印推荐的古诗词
void print_recommended_poetry(cJSON *data) {
    // 获取 "content" 字符串
    cJSON *content = cJSON_GetObjectItemCaseSensitive(data, "content");
    if (!cJSON_IsString(content) || (content->valuestring == NULL)) {
        fprintf(stderr, "诗词 JSON 格式错误: 找不到 'content' 字符串\n");
        return;
    }

    // 打印推荐的古诗词
    printf("推荐的古诗词：%s\n", content->valuestring);
}

// 打印天气信息
void print_weather_info(cJSON *data) {
    // 获取 "weatherData" 对象
    cJSON *weatherData = cJSON_GetObjectItemCaseSensitive(data, "weatherData");
    if (!cJSON_IsObject(weatherData)) {
        fprintf(stderr, "info JSON 格式错误: 找不到 'weatherData' 对象\n");
        return;
    }

    // 获取天气相关信息
    cJSON *temperature = cJSON_GetObjectItemCaseSensitive(weatherData, "temperature");
    cJSON *weather = cJSON_GetObjectItemCaseSensitive(weatherData, "weather");
    cJSON *windDirection = cJSON_GetObjectItemCaseSensitive(weatherData, "windDirection");
    cJSON *windPower = cJSON_GetObjectItemCaseSensitive(weatherData, "windPower");
    cJSON *humidity = cJSON_GetObjectItemCaseSensitive(weatherData, "humidity");
    cJSON *updatetime= cJSON_GetObjectItemCaseSensitive(weatherData, "updateTime");

    if (!cJSON_IsNumber(temperature) || !cJSON_IsString(weather) || (weather->valuestring == NULL) ||
        !cJSON_IsString(windDirection) || (windDirection->valuestring == NULL) || !cJSON_IsNumber(windPower) ||
        !cJSON_IsNumber(humidity)||!cJSON_IsString(updatetime)) {
        fprintf(stderr, "info JSON 格式错误: 找不到完整的天气信息\n");
        return;
    }

    // 打印天气信息描述
    printf("当前天气：%s, 气温: %.1f°C, 风向: %s, 风力: %d级, 湿度: %d%%,现在时间：%s\n",
           weather->valuestring, temperature->valuedouble, windDirection->valuestring,
           windPower->valueint, humidity->valueint,updatetime->valuestring);
}

// 用于处理CURL响应的写回调函数
size_t write_callback(void *ptr, size_t size, size_t nmemb, void *stream) {
    size_t realsize = size * nmemb;
    FILE *memstream = (FILE *)stream;
    size_t written = fwrite(ptr, size, nmemb, memstream);
    return written;
}

int main(void) {
    CURL *client;
    CURLcode err;
    char *poetry_response = NULL;
    char *info_response = NULL;
    size_t poetry_size, info_size;

    // 创建内存流
    FILE *poetry_memstream = open_memstream(&poetry_response, &poetry_size);
    FILE *info_memstream = open_memstream(&info_response, &info_size);
    if (poetry_memstream == NULL || info_memstream == NULL) {
        perror("open_memstream");
        return 1;
    }

    // 初始化 CURL
    curl_global_init(CURL_GLOBAL_ALL);
    client = curl_easy_init();

    // 设置 CURL 选项调用诗词 API
    curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/sentence");
    curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, write_callback);
    curl_easy_setopt(client, CURLOPT_WRITEDATA, poetry_memstream);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "X-User-Token:XBP72f35wewvt7VufUp9Lvi9H4E6mbdO"); // 替换 YOUR_API_KEY 为你的 API Key
    curl_easy_setopt(client, CURLOPT_HTTPHEADER, headers);

    // 执行 CURL 请求获取诗词
    err = curl_easy_perform(client);
    fclose(poetry_memstream); // 关闭内存流

    if (err != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() 失败: %s\n", curl_easy_strerror(err));
    } else {
        // 获取诗词 JSON 中的 data 对象
        cJSON *poetry_data = get_data_object(poetry_response, poetry_size);
        if (poetry_data != NULL) {
            // 打印推荐的古诗词
            print_recommended_poetry(poetry_data);
            cJSON_Delete(poetry_data);
        }

        // 设置 CURL 选项调用 info API
        curl_easy_setopt(client, CURLOPT_URL, "https://v2.jinrishici.com/info");
        curl_easy_setopt(client, CURLOPT_WRITEDATA, info_memstream);

        // 执行 CURL 请求获取info
        err = curl_easy_perform(client);
        fclose(info_memstream); // 关闭内存流

        if (err != CURLE_OK) {
            fprintf(stderr, "curl_easy_perform() 失败: %s\n", curl_easy_strerror(err));
        } else {
            // 获取info JSON 中的 data 对象
            cJSON *info_data = get_data_object(info_response, info_size);
            if (info_data != NULL) {
                // 打印天气信息
                print_weather_info(info_data);
                cJSON_Delete(info_data);
            }
        }
    }

    // 清理资源
    curl_slist_free_all(headers);
    curl_easy_cleanup(client);
    free(poetry_response);
    free(info_response);
    curl_global_cleanup();

    return 0;
}