#ifndef TOKEN_H
#define TOKEN_H

//成功返回access token，失败返回NULL
char *get_access_token(const char *ak, const char *sk);

#endif